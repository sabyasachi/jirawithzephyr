package Test;




import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;


public class CreateTopic {

	static boolean status = false;
	
	@BeforeTest
	public void start()  {
	
		System.setProperty("webdriver.ie.driver", "D:\\sabya\\IEChrome\\IEDriverServer.exe");
		Driver.driver = new InternetExplorerDriver();
		Driver.driver.manage().window().maximize();
		Driver.driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		
		
		/*
//For IE Browser
	  	DesiredCapabilities capabilities = DesiredCapabilities.internetExplorer();
	  	capabilities.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS, true);
	  	System.setProperty("webdriver.ie.driver", "..\\KaamsNG\\IEDriverServer.exe");
	  		
	  	Driver.driver = new InternetExplorerDriver(capabilities);
	    */
	    
		//Driver.driver = new FirefoxDriver();
	}
	
	
	@Test
	public void CreateTopic()  {

   try {
	   Logger Log = Logger.getLogger(CreateTopic.class);
		
	   Driver.driver.get(GetData.getTestData("url"));
	    WebDriverWait wait = new WebDriverWait(Driver.driver, 20);
	    JiraStepUpdate.updateStep(0, 1,"Sucessfully Navigated to Kaams-Ng portal");
	    //Jira.takeSnap(0, "Kaams_ng", Driver.driver);
	     //Explicit wait, script will wait max 20 sec's, If element visible before that it will be proceed.
	    try {
	    	wait.until(ExpectedConditions.visibilityOf(Driver.driver.findElement(By.id(GetData.getLocator("username")))));
		    Driver.driver.findElement(By.id(GetData.getLocator("username"))).sendKeys(GetData.getTestData("Username"));
		    System.out.println("Username entered successfully");
		  	Log.info("_______Username entered successfully_______");
		  	
		} catch (Exception e) {
			
			
		}
	  	
	   try {
		   Driver.driver.findElement(By.id(GetData.getLocator("password"))).sendKeys(GetData.getTestData("Password"));
		    System.out.println("Password entered successfully");
		  	Log.info("_______Password entered successfully_______");
		  	JiraStepUpdate.updateStep(1, 1,"Sucessfully loged in");
		  	//Jira.takeSnap(1, "login", Driver.driver);
	} catch (Exception e) {
		
	}
        

		//Driver.driver.findElement(By.linkText("Log On")).click();
		Driver.driver.findElement(By.className(GetData.getLocator("loginButton"))).click();
		System.out.println("LogOn button clicked on successfully");
		  Log.info("_______LogOn button clicked on successfully_______");
		  JiraStepUpdate.updateStep(2, 1,"Sucessfully opened library page");
		  //Jira.takeSnap(2, "libraryPage", Driver.driver);
		  Thread.sleep(5000);
		  /*
		   * validation for user
		   */
		  
		  String loggedUserName=Driver.driver.findElement(By.xpath(GetData.getLocator("Logged username"))).getText();
		  Assert.assertEquals(loggedUserName, GetData.getTestData("Username"));
		  System.out.println("Loggedin successfully");
		  Log.info("_______Loggedin successfully_______");
		  
		  ((JavascriptExecutor)Driver.driver).executeScript("arguments[0].scrollIntoView(true);", Driver.driver.findElement(By.xpath(GetData.getLocator("existingLibrary"))));
		  wait.until(ExpectedConditions.visibilityOf(Driver.driver.findElement(By.xpath(GetData.getLocator("existingLibrary")))));
		  Driver.driver.findElement(By.xpath(GetData.getLocator("existingLibrary"))).click();
		  System.out.println("Clicked on existing Library for updating Topic");
		  Log.info("_______Clicked on existing Library for updating Topic_______");
		  
		  
		  //wait.until(ExpectedConditions.visibilityOf(Driver.driver.findElement(By.xpath(".//*[@id='topicViewDiv']/div[2]/div[2]/div/div[1]/table/tbody/tr/td[2]"))));
		 Thread.sleep(5000);
		  Driver.driver.findElement(By.xpath(GetData.getLocator("editIcon"))).click();
		  System.out.println("Edit icon clicked on successfully");
		  Log.info("_______Edit icon clicked on successfully_______");
		  JiraStepUpdate.updateStep(3, 1,"Sucessfully opened edit library page");
		  //Jira.takeSnap(3, "editlibraryPage", Driver.driver);
		
		   Thread.sleep(10000);
		  Driver.driver.switchTo().frame(GetData.getLocator("iframeplayer"));
		  //wait.until(ExpectedConditions.visibilityOf(Driver.driver.findElement(By.xpath(GetData.getLocator("Edit Library Page")))));
		  Thread.sleep(2000);
		  int page=Driver.driver.findElements(By.xpath(GetData.getLocator("Manage Topic Icon"))).size();
		  if (page!=0) {
			  System.out.println("Successfully navigated to Edit Library Page");
			  Log.info("_______Successfully navigated to Edit Library Page_______");
			  
		} else {
			throw new Exception("Edit Libray page Failed to load");

		}
		  
		  //Clicking on Manage Topics Icon
		  Driver.driver.findElement(By.xpath(GetData.getLocator("Manage Topic Icon"))).click();
		  System.out.println("'Manage Topics' icon clicked on successfully");
		  Log.info("_______'Manage Topics' icon clicked on successfully_______");
		  
		  JiraStepUpdate.updateStep(4, 1,"Sucessfully opened Manage topic page");
		  //Jira.takeSnap(4, "managetopic", Driver.driver);
		  int addMoreDetails=Driver.driver.findElements(By.xpath(GetData.getLocator("Add More Details"))).size();
		  if (addMoreDetails!=0) {
			  System.out.println("Add new topic page Navigated sucessfully");
			  Log.info("_______Add new topic page Navigated sucessfully_______");
		} else {
			throw new Exception("Add new topic page Failed to load");

		}
			  
	//Enter text in the Title field
		  Thread.sleep(10000);
		  //Driver.driver.findElement(By.id("TopicTitle")).sendKeys("TestNewTopic");
		  Driver.driver.findElement(By.xpath(GetData.getLocator("Title updated"))).sendKeys(GetData.getTestData("Title updated"));
		  System.out.println("Title updated for New Topic");
		  Log.info("_______Title updated for New Topic_______");
		  
		  
	//Selecting Type dropdown option selection
				Select dropdownType = new Select(Driver.driver.findElement(By.xpath(GetData.getLocator("ManageTopic dropdown"))));
				dropdownType.selectByVisibleText("Generic");
				System.out.println("'Generic' type option selected from dropdown options");
				Log.info("_______'Generic' type option selected from dropdown options_______");
				
	 //Enter text in the Short Description field
		  
		  Thread.sleep(10000);
		  Driver.driver.findElement(By.xpath(GetData.getLocator("Description"))).sendKeys(GetData.getTestData("description"));
		  System.out.println("Short Description updated for New Topic");
		  Log.info("_______Short Description updated for New Topic_______");
		  
		  JiraStepUpdate.updateStep(5, 1,"Sucessfully entered topic details");
		 // Jira.takeSnap(5, "topicdetails", Driver.driver);
		
	//Clicking on Save Info Button
			 Thread.sleep(5000);
			Driver.driver.findElement(By.xpath(GetData.getLocator("Save Info Button"))).click();
			System.out.println("'Save Info' button clicked on successfully");
			Log.info("_______'Save Info' button clicked on successfully_______");
			JiraStepUpdate.updateStep(6, 1,"Sucessfully added new topic");
			//Jira.takeSnap(6, "newtopic", Driver.driver);
} catch (Exception e) {
	e.printStackTrace();
	e.getCause();
}
	  
	  
	}
	
	@AfterTest
	public void after(){
		Driver.driver.close();
		
	}
}
