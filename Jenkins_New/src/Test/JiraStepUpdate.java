package Test;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.Set;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.FileUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.ContentBody;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;

public class JiraStepUpdate {
	/*
	 * Method for Retriving Summary and Execution Id of Test case
	 * Method parameter-- cycleId
	 */
	public static Hashtable<String, String> getCycle(String cycleId){
		Hashtable<String, String> cycleDetails=new Hashtable<>();
		String[] testCaseSummary=null;
		String execution[] = null;
	try {
		final String BASE_URL = "http://192.168.11.41:8080";
	    final String ZAPI_URL = BASE_URL + "/rest/api/2/project";
	 final String CREDENTIALS = JiraListner.getProperties("JiraCredentials");
	 final String encoding = new Base64().encodeToString(CREDENTIALS.getBytes());
	 String retexecution=BASE_URL+"/rest/zapi/latest/execution?cycleId="+cycleId;
        HttpURLConnection rconn=(HttpURLConnection) new URL(retexecution).openConnection();
        rconn.setDoInput(true);
        rconn.setDoOutput(true);
        rconn.setRequestMethod("GET");
        rconn.setRequestProperty("Authorization", "Basic "+encoding);
        rconn.setRequestProperty("Content-type", "application/json");
        JSONObject obj8=new JSONObject();
       
        BufferedReader br8=new BufferedReader(new InputStreamReader(rconn.getInputStream()));
       
      String esec=br8.readLine();
     // System.out.println(esec);
      
      JSONObject root = new JSONObject(esec);
      JSONArray sportsArray = root.getJSONArray("executions");
     
     // System.out.println(sportsArray.length());
      testCaseSummary=new String[sportsArray.length()];
      execution=new String [sportsArray.length()];
      
      for (int i = 0; i < sportsArray.length(); i++) {
    	  JSONObject firstSport = sportsArray.getJSONObject(i);
    	  String name = firstSport.getString("cycleName"); 
	      int id = firstSport.getInt("cycleId");
	     // JSONArray leaguesArray = firstSport.getJSONArray("leagues");
         String executionId=firstSport.getString("id");
         String issueId=firstSport.getString("issueId");
         String summary=firstSport.getString("summary");
         testCaseSummary[i]=firstSport.getString("summary");
         execution[i]=firstSport.getString("id");
	     cycleDetails.put(summary, executionId);
        System.out.println("Cycle name :"+name +"  "+"CycleId:"+id+" "+"Execution Id  :"+executionId+"  "+" Issue Id  :"+issueId+" "+"summary  :"+summary);
    
      }
      br8.close();
      
      } catch (Exception e) {
		e.printStackTrace();
	}
	//System.out.println(cycleDetails.get("CreateTopic"));
	return cycleDetails;
	
	}
	
	/*
	 * Method for Retriving all test step details based on cycleId
	 */
	public static String[] getStepDetails(){
		String[] step = null ;
		String executionId="";
		System.out.println(JiraListner.getProperties("TestCycleId"));
		Hashtable<String, String> arr=getCycle(JiraListner.getProperties("TestCycleId"));
		Set<String> st=arr.keySet();
		//Iterator<String> it=st.iterator();
		 for(String key: st){
	            
	            if (key.equalsIgnoreCase(JiraListner.getMethodName())) {
	   			 executionId=arr.get(JiraListner.getMethodName());
	   			 System.out.println("Initializing Execution Id");
	   			System.out.println("Execution Id of "+key+" is: "+arr.get(key));
	   			 System.out.println(executionId);
	   			 
	   			 
	   				try {
	   					
	   					final String BASE_URL = "http://192.168.11.41:8080";
	   				    final String ZAPI_URL = BASE_URL + "/rest/api/2/project";
	   				 final String CREDENTIALS = JiraListner.getProperties("JiraCredentials");
	   				 
	   				 final String encoding = new Base64().encodeToString(CREDENTIALS.getBytes());
	   				 String retexecution22=BASE_URL+"/rest/zapi/latest/stepResult?executionId="+executionId;
	   				 System.out.println("Step.....1");
	   				// String retexecution22=BASE_URL+"/rest/zapi/latest/stepResult/"+executionId;
	   			        HttpURLConnection rconn12=(HttpURLConnection) new URL(retexecution22).openConnection();
	   			        rconn12.setDoInput(true);
	   			        rconn12.setDoOutput(true);
	   			        rconn12.setRequestMethod("GET");
	   			        rconn12.setRequestProperty("Authorization", "Basic "+encoding);
	   			        rconn12.setRequestProperty("Content-type", "application/json");
	   			        JSONObject obj9=new JSONObject();
	   			       
	   			        BufferedReader br9=new BufferedReader(new InputStreamReader(rconn12.getInputStream()));
	   			       
	   			      String esec1=br9.readLine();
	   			      System.out.println(esec1);
	   			      System.out.println("Response returned from server........");
	   			   System.out.println("Step.....2");
	   			      JSONArray arr1=new JSONArray(esec1);
	   			      step=new String[arr1.length()];
	   			   System.out.println("Step.....3");
	   			      for (int i1 = 0; i1 < arr1.length(); i1++) {
	   						JSONObject proj = arr1.getJSONObject(i1);
	   						String stepId=proj.getString("id");
	   						//String orderId=proj.getString("orderId");
	   						System.out.println("ID: "+proj.getString("id"));
	   						step[i1]=proj.getString("id");
	   						System.out.println("Updating array for..."+JiraListner.getMethodName());
	   			      }
	   			      br9.close();
	   			   System.out.println("Step.....4");
	   				} catch (Exception e) {
	   					e.printStackTrace();
	   				}
	   			
	   		}
	        }
		 System.out.println("Step.....5");
		return step;
	}
	
	/*
	 * Method for updating step results
	 * 
	 */
	public static void updateStep(int stepIndex,int status,String comment){
try {
	System.out.println(JiraListner.getMethodName());
	String testName=JiraListner.getMethodName();
	System.out.println("inside step update method.........................");
	String[] stepId=getStepDetails();
	final String BASE_URL = "http://192.168.11.41:8080";
   
    final String CREDENTIALS = JiraListner.getProperties("JiraCredentials");
 
     final String encoding = new Base64().encodeToString(CREDENTIALS.getBytes());
	 String retexecution1=BASE_URL+"/rest/zapi/latest/stepResult/"+stepId[stepIndex];
     HttpURLConnection rconn1=(HttpURLConnection) new URL(retexecution1).openConnection();
     rconn1.setDoInput(true);
     rconn1.setDoOutput(true);
     rconn1.setRequestMethod("PUT");
     rconn1.setRequestProperty("Authorization", "Basic "+encoding);
     rconn1.setRequestProperty("Content-type", "application/json");
     int status1=status;
 	  JSONObject passObj=new JSONObject();
 	  System.out.println("Updating status....."+testName+stepId[stepIndex]+1);
 	  passObj.put("status", String.valueOf(status1));
 	  passObj.put("comment", comment);
       OutputStreamWriter passOut=new OutputStreamWriter(rconn1.getOutputStream());
       passOut.write(passObj.toString());
       passOut.flush();
       passOut.close();
     
    
     BufferedReader br91=new BufferedReader(new InputStreamReader(rconn1.getInputStream()));
    
   String esec111=br91.readLine();
   System.out.println(esec111);
   br91.close();
   String retexecution22=BASE_URL+"/rest/zapi/latest/attachment/attachmentsByEntity?entityId="+stepId[stepIndex] + "&entityType=TESTSTEPRESULT";
   System.out.println("Step.....1");
    HttpURLConnection rconn12=(HttpURLConnection) new URL(retexecution22).openConnection();
    rconn12.setDoInput(true);
    rconn12.setDoOutput(true);
    rconn12.setRequestMethod("GET");
    rconn12.setRequestProperty("Authorization", "Basic "+encoding);
    rconn12.setRequestProperty("Content-type", "application/json");
    BufferedReader br9=new BufferedReader(new InputStreamReader(rconn12.getInputStream()));
    String delete=br9.readLine();
    System.out.println(delete);
    JSONObject root = new JSONObject(delete);
    JSONArray sportsArray = root.getJSONArray("data");
    for (int i = 0; i < sportsArray.length(); i++) {
 	  JSONObject firstSport = sportsArray.getJSONObject(i);
 	  System.out.println(firstSport);
 	  String fileId=firstSport.getString("fileId");
        System.out.println(fileId);
        String retexecution221=BASE_URL+"/rest/zapi/latest/attachment/"+fileId;
		 System.out.println("Deleting attachments........");
		// String retexecution22=BASE_URL+"/rest/zapi/latest/stepResult/"+executionId;
	        HttpURLConnection rconn121=(HttpURLConnection) new URL(retexecution221).openConnection();
	        rconn121.setDoInput(true);
	        rconn121.setDoOutput(true);
	        rconn121.setRequestMethod("DELETE");
	        rconn121.setRequestProperty("Authorization", "Basic "+encoding);
	        rconn121.setRequestProperty("Content-type", "application/json");
	        BufferedReader brDel=new BufferedReader(new InputStreamReader(rconn121.getInputStream()));
	        System.out.println(brDel);
   }
   File scrFile = ((TakesScreenshot)Driver.driver).getScreenshotAs(OutputType.FILE);
   String destDir =JiraListner.getProperties("JiraSnapshot");
   Calendar cal = Calendar.getInstance();
	cal.add(Calendar.DATE, 1);
	SimpleDateFormat format1 = new SimpleDateFormat("dd-MM-yyyy_HH_mm_ss");
	String destFile="Screenshot"+".png";
	 try {
			FileUtils.copyFile(scrFile, new File(destDir + "/" + destFile));
		} catch (IOException e) {
			e.printStackTrace();
		}
	
		 File filePass=new File(JiraListner.getProperties("JiraSnapshot")+"\\"+destFile);
		 
			
		 final HttpPost httpPost =
                new HttpPost(BASE_URL+"/rest/zapi/latest/"+ "attachment?entityId=" + stepId[stepIndex]
                        + "&entityType=TestStepResult");
       
        httpPost.setHeader("X-Atlassian-Token", "nocheck");
        httpPost.setHeader("Authorization", "Basic " + encoding);
 
        final MultipartEntity mpEntity = new MultipartEntity(HttpMultipartMode.BROWSER_COMPATIBLE);
        final ContentBody cbFile = new FileBody(filePass);
        mpEntity.addPart("file", cbFile);
        httpPost.setEntity(mpEntity);
        final HttpResponse response = new DefaultHttpClient().execute(httpPost);
        final HttpEntity resEntity = response.getEntity();
 
        if (null != resEntity) {
            EntityUtils.consume(resEntity);
        }
} catch (Exception e) {
	e.printStackTrace();
}
	}
	
	
	
	
	

	
}
